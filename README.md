# Overview
my-financials is a PHP/Mysql webapp used to manage personal finances. It can run on your local machine, or hosted on the web. It has been tested with the Apache and Nginx, and it can be run on Windows, MacOS, or Linux by following the instructions below.


## Features

* Manage your Accounts (Checking, Saving, Brokerage, Loans, Currency)
* Manage your Transactions
  * Withdrawals, Deposits and Transfers
  * Mortgage / Loan payments
  * Stock purchases or sales
  * Categorize and tag your transactions for reporting and analysis
  * Keyword searching and time period filtering
* Store asset images and transaction receipts
* Make a monthly budget that can update in real time 
* Interactive reporting suite. Report on:
    * Transaction categories
    * Transaction payees
    * Asset allocation
    * Net Worth
    * Gas Prices
    * Monthly/year expense breakdowns
* Full featured automotive plugin:
  * Track vehicle maintenance history and expenses
  * Track your gas mileage
  * Associate insurance and tax payments to a vehicle
  * Calculate your total cost of ownership (TCO)
* Plug-in system that allows developers to add their own features
* Multi user support


## Debian-based Linux (Apache) Install process

1. Install the dependencies
  > sudo apt-get install apache2 libapache2-mod-php7.1 php7.1-curl php7.1-mysql composer mysql-client-core-5.7 mysql-server-5.7 git

2. Enable mod_rewrite
  > sudo a2enmod rewrite
  
  > sudo systemctl restart apache2

3. Clone the project 
  > git clone https://gitlab.com/toddhudgens/my-financials
  
  > cd my-financials
  
4. Run the install.sh script
  > chmod +x install.sh
  
  > ./install.sh

5. In your apache config, point the DocumentRoot directive to the www/ folder in the project. Ensure that directive "AllowOverride" is set to "All". 

6. Restart apache and browse to the site you just configured. It should be working!

## Debian-based Linux (Nginx) Install Process

1. TODO

## MacOS Homebrew Install Process

1. Install the homebrew package manager (Instructions at https://brew.sh/)

2. Install PHP 7.3
  > brew install php@7.3

## MacOS MAMP Install Process

1. TODO

## Windows Install Process

1. TODO


## Attributions

my-financials makes use of the Silk Icons from famfamfam.com (http://www.famfamfam.com/lab/icons/silk/)
