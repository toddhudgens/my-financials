$( document ).ready(function() {
  $('input#searchText').keypress(function (e) {
    if(e.keyCode == 13) { transactionSearch(); }
  });
});

$(window).resize(function() { redrawIncomeVsExpenseChart(); });


function showAccountActions(id) {
  $('#editAccount'+id).show();
}

function hideAccountActions(id) {
  $('#editAccount'+id).hide();
}


function clearAccountInputs() {
  $('#accountName').val('');
  $('#initialBalance').val('');
  $('#accountType').val('');
  $('#assetSearch').val('');
  $('#associatedAssetRow').hide();
  $('#liquid').attr('checked', false);
  $('#accountActive').attr('checked', false);
}

function showNewAccountForm() {
  clearAccountInputs();

  $('#accountId').val('');

  $('#editAccountForm').dialog({
    modal: true,
    title: "Add New Account",
    width:600
  });
}

function editAccount(id) {
  clearAccountInputs();
  $('#accountId').val(id);
  $('#accountName').val($('#accountName'+id).val());
  $('#initialBalance').val($('#initialBalance'+id).val());

  var accountType = $('#accountType'+id).val();
  $('#accountType').val(accountType);

  if ((accountType == "4") || (accountType == "6")) { 
    $('#associatedAssetRow').show();
  }

  var liquid = false;
  if ($('#liquid'+id).val() == 1) { liquid = true; }
  $('#liquid').prop('checked', liquid);

  var active = false;
  if ($('#accountActive'+id).val() == 1) { active = true; }
  $('#accountActive').prop('checked', active);

  $('#assetSearch').val($('#assetName'+id).val());
  $('#assetId').val($('#assetId'+id).val());

  $('#notes').val($('#notes'+id).val());

  $('#editAccountForm').dialog({
    modal: true,
    title: "Edit Account",
    width:600
  });
}


function updateAccountType() {
  var accountType = $('#accountType').val();
  if ((accountType == "Loan") || (accountType == "Mortgage")) { 
    $('#associatedAssetRow').show();
  }
  else { $('#associatedAssetRow').hide(); }
}


function saveAccount() {
  var liquid = 0; var active = 0;
  if ($('#liquid').is(":checked")) { liquid = 1; }
  if ($('#accountActive').is(":checked")) { active = 1; }

  var url = 
   '/account/save?' + 
    'accountId=' + $('#accountId').val() + '&' +
    'name=' + encodeURIComponent($('#accountName').val()) + '&' + 
    'initialBalance=' + $('#initialBalance').val() + '&' +
    'accountType=' + $('#accountType').val() + '&' + 
    'notes=' + encodeURIComponent($('#notes').val()) + '&' + 
    'assetId=' + $('#assetId').val() + '&' +
    'liquid=' + liquid + '&' +  
    'active=' + active;
  jQuery.ajax({
    url: url,
    success: function(data) { 
      var results = jQuery.parseJSON(data);
      if (results[0] === "success") {
        $('editAccountForm').dialog('close');
        window.location.reload();
      }
      else { alert(data); }
    }
  });
}


function transactionSearch() {
  var search = $('#searchText').val();
  window.location = '/transaction/search?s='+search;
}


function redrawIncomeVsExpenseChart() {
  var pageWidth = $(window).width();
  var chartWidth = 0; chartHeight = 0;

  // this code should not be necessary, however I was not able to get media queries
  // to work with the jquery flot charts
  if (pageWidth < 768) {
    chartWidth = 450; chartHeight = 300;
  }
  else if (pageWidth < 992) {
    chartWidth = 300; chartHeight = 225;
  }
  else if (pageWidth < 1200) {
    chartWidth = 420; chartHeight = 300;
  }
  else {
    chartWidth = 500; chartHeight = 350;
  }
  $('#incomeVsExpensesChart').width(chartWidth).height(chartHeight);


  var options = {
    grid: { hoverable: true },
    series: { stack: 0,
              lines: { show: false, steps: false },
              bars: { show:true, barWidth: 0.9, align: 'center' }
    },
    tooltip: true,
    tooltipOpts: {
      content: function(label, xval, yval) { return "$" + yval; },
      shifts: { x:-40, y: 25 }
    },
    yaxis: {
      tickFormatter: function(val, axis) { return "$" + val; },
      tickLength:0
    },
    xaxis: { tickLength:0, ticks: [[0, 'Income'], [1, 'Expenses']]}
  };

  $.plot($("#incomeVsExpensesChart"), window.incomeVsExpenseData, options);
}
