function validateNewUserForm() {
  var pw1 = $('#password1').val();
  var pw2 = $('#password2').val();
  if (pw1 !== pw2) {
    $('#pwMismatch').css('visibility','visible').hide().fadeIn("slow");
  }
  else {
    $('#pwMismatch').css('visibility','hidden').hide();
    $('#init-user-form').submit();
  }
}
