function capitalize(str) {
  return str.charAt(0).toUpperCase() + str.slice(1);
}


function displayUploadedImages(type,response) {
  if (response.result == "success") {
    var picturesDiv = $('#'+type+'Pictures');
    var newHTML = '';
    for (var i = 0; i < response.addedImages.length; i++) {
      var img = response.addedImages[i];
      newHTML += imageHTML(type, img.id, img.filename);
    }
    picturesDiv.append(newHTML);
  }
}


function imageClick(type,image) {
  var modal = document.getElementById(type+"ImageModal");
  var modalImg = document.getElementById(type+"ImageModalContent");
  var captionText = document.getElementById(type+"ImageModalCaption");

  modal.style.display = "block";
  modalImg.src = image.src;
  captionText.innerHTML = image.alt;
  window[type+'ImageModalVisible'] = 1;
  console.log("imageClick(), " + type + ", " + window[type+"ImageModalVisible"]);
  $("#edit"+capitalize(type)+"Form").dialog( "option", "closeOnEscape", false);
}


function imageDelete(type,element) {
  var pictureId = element.getAttribute('pictureid');

  var result = confirm("Are you sure you want to delete this picture?");
  if (result) {
    var url = '/picture-delete?id='+pictureId;
    jQuery.ajax({
      url: url,
      success: function(data) {
        var response = jQuery.parseJSON(data);
        if (response['result'] == "success") {
          $('#'+type+'Image'+pictureId).remove();
          $('#'+type+'ImageDel'+pictureId).remove();
        }
        else { alert("Error deleting picture!"); }
      }
    });
  }
}


function imageModalClose(type) {
  var modal = document.getElementById(type+"ImageModal");
  modal.style.display = 'none';

  window[type+'ImageModalVisible'] = 0;
  $("#edit"+capitalize(type)+"form").dialog( "option", "closeOnEscape", true);
}


function imageHTML(type,id, filename) {
  return '<img id="'+type+'ImageDel'+id+'" class="'+type+'ImageDelete" pictureId="'+id+'" '+
           'src="/images/close_32.png" onclick="imageDelete(\''+type+'\',this)">' +
         '<img id="'+type+'Image' + id + '" '+
           'src="/images/'+type+'s/' + filename + 
           '" class="'+type+'Image" '+
           'onclick="imageClick(\''+type+'\',this)" width="160">';
}
