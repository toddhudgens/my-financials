var pollInterval = 30000;
var dialogShown = 0;


function initSessionMonitor() {
  // every N seconds, check user session on server
  setInterval('pollForSessionLifespan()', pollInterval)
}


function logout() { window.location.href = '/user/logout'; }


function stayLoggedIn() {
  var url = '/user/session-update';
  $.ajax({
    url: url,
    success: function(result) {
      dialogShown = 0;
      delete window.sessionExpirationTS;
      $('#timeoutDialog').dialog('close');
    }
  });
}


function updateSecondsRemaining() {
  if (window.sessionExpirationTS === undefined) { return; }
  var now = Math.round((new Date()).getTime() / 1000);
  var secondsRemaining = window.sessionExpirationTS - now;
  if (secondsRemaining <= 0) {
    $('#secondsRemaining').html('0');
    logout();
  }
  else {
    $('#secondsRemaining').html(secondsRemaining);
  }
}


function showSessionTimeoutDialog(secondsRemaining) {
  $('#secondsRemaining').html(secondsRemaining);
  $('#timeoutDialog').dialog({
    height:200,
    width:550,
    modal:true,
    buttons: {
      "Log Out": logout,
      "Stay Logged In": stayLoggedIn
    }
  });
  var now = Math.round((new Date()).getTime() / 1000);
  var expiration = parseInt(now) + parseInt(secondsRemaining);
  window.sessionExpirationTS = expiration;
  setInterval('updateSecondsRemaining()', 1000)
}


function pollForSessionLifespan() {
  var url = '/user/session-check';
  $.ajax({
    url: url,
    success: function(result) {
      if (result == "false") { logout(); }
      else if (result <= 90) {
        if (dialogShown == 0) {
          showSessionTimeoutDialog(result);
          dialogShown = 1;
        }
        else {
          $('#secondsRemaining').html(result);
        }
      }
      else {
        if (dialogShown) {
          $('#timeoutDialog').dialog('close');
          dialogShown = 0;
        }
      }
    },
    error: function() { logout(); }
  });
}
