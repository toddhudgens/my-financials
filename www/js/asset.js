var mode = '';
var assetId = 0;

$(document).ready(function() {
  $('#sold').change(function() { soldToggled(); });    

  $("#pictureUpload").change(function(){
    var files = this.files;
    const formData = new FormData()
    for (let i = 0; i < files.length; i++) {
      let file = files[i];
      formData.append('files[]', file)
    }

    let url = '/asset/picture-upload?entityId='+assetId;
    fetch(url, {
      method: 'POST',
      body: formData,
    }).then(response =>response.json())
      .then(data => { displayUploadedImages('asset',data); });
  });


  // escape key to exit image modal
  $(document).keyup(function(e) {
    if (e.keyCode === 27) {
      if (window.assetImageModalVisible) { imageModalClose('asset'); }
      else { $('#editAssetDialog').dialog('close'); }
    }
  });

});


function showAssetActions(id) { 
 $('#edit'+id).show(); 
 $('#delete'+id).show();
}

function hideAssetActions(id) { 
 $('#edit'+id).hide(); 
 $('#delete'+id).hide();
}

function showAssetEditForm(id) {
  assetId = id;
  mode = 'edit';

  var asset = window.assets[id];
  $('input#assetName').val(asset.name);
  $('input#assetCategorySearch').val(asset.category);
  $('input#assetCategoryId').val(asset.categoryId);
  $('input#currentValue').val(asset.currentValue);
  $('input#purchasePrice').val(asset.initialValue);
  $('#madeIn').val(asset.countryId);
  $('textarea#notes').html('');
  $('textarea#notes').html(asset.notes);
  $('#dateSold').val(asset.dateSold);
    
  if (asset.sold == 1) {
    $('#sold').prop('checked', true);
    $('#dateSoldRow').show();
  }
  else {
    $('#sold').prop('checked', false);
    $('#dateSoldRow').hide();
  }

  var assetPicturesDiv = $('#assetPictures');
  var pictures = [];
  if (asset.pictures !== undefined) {
    pictures = asset.pictures.split(',');
  }

  var newHTML = '';

  for (var i = 0; i < pictures.length; i++) {
    var pictureId = pictures[i].split('.').slice(0,-1);
    newHTML += imageHTML('asset',pictureId, pictures[i]);
  }
  assetPicturesDiv.html(newHTML);

  $('#editAssetForm').dialog({
    modal: true,
    title: "Edit Asset",
    closeOnEscape: true,
    width:800
  });

  // trigger event for plugins to update view if needed
  var event = jQuery.Event("editAssetFormShown");
  event.assetId = id;
  event.categoryId = asset.categoryId;

  $("body").trigger(event);
}


function showNewAssetForm() {
  $('#assetName').val('');
  $('#assetCategorySearch').val('');
  $('#assetCategoryId').val('');
  $('#purchasePrice').val('');
  $('#currentValue').val('');
  $('#notes').html('');
  $('#madeIn').val('');
  $('#assetPictures').html('');

  $('#sold').prop('checked', false);
  assetId = 0;
  mode = 'new';
  $('#editAssetForm').dialog({
    modal: true,
    title: "Add New Asset",
    width:600
  });

  var event = jQuery.Event("addAssetFormShown");
  $("body").trigger(event);
}


function saveAsset() {
  if ($('#sold')[0].checked) { sold = 1; } else { sold = 0; }

  window.ajaxURL = '/asset/save?';
  window.ajaxParams =
     'mode=' + mode + '&' +
     'assetId=' + assetId + '&' +
     'name=' + encodeURIComponent($('input#assetName').val()) + '&' +
     'category=' + $('input#assetCategoryId').val() + '&' +
     'categoryName='+encodeURIComponent($('input#assetCategorySearch').val())+'&' +
     'madeIn='+$('#madeIn').val() + '&' +
     'currentValue=' + $('input#currentValue').val() + '&' +
     'purchasePrice=' + $('input#purchasePrice').val() + '&' +
     'sold=' + sold + '&' +
     'dateSold=' + $('input#dateSold').val() + '&' +
     'notes=' + $('textarea#notes').val();

  // trigger event for plugins to append additional GET vars if needed
  var event = jQuery.Event("saveAsset");
  $("body").trigger(event);

  jQuery.ajax({
    url: window.ajaxURL + window.ajaxParams,
    success: function(data) {
      var results = jQuery.parseJSON(data);
      if (results[0] === "success") {
        $('editAssetForm').dialog('close');
	window.location.reload();
      }
      else { alert(data); }
    }
  });
}


function deleteAsset(id) {
  if (confirm("Are you sure you want to delete this asset?")) {
    var url = '/asset/delete?id='+id;
    jQuery.ajax({
      url: url,
      success: function(data) {
        var results = jQuery.parseJSON(data);
        if (results[0] === "success") { window.location.reload(); }
        else { alert(data); }
      }
    });
  }
}


function soldToggled() {
  if ($('#sold').is(':checked')) {
    $('#dateSoldRow').show();
  }
  else {
    $('#dateSoldRow').hide();
  }
}







