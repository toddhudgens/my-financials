$(document).ready(function() { 

  updateYearSelectionVisibility($('#dateRange').val());
  plotExpenseChart(); 

  $("#dateRange").change(function() { 
    updateYearSelectionVisibility(this.value); 
  });

});
$(window).resize(function() { plotExpenseChart(); });


function updateYearSelectionVisibility(dateRange) {
  if (dateRange == "single-year") { $('#yearSelection').show(); }
  else { $('#yearSelection').hide(); }

}

function runExpenseReport() {
  console.log("runExpenseReport()");
  var url = '/report/expenses';
  var dateRange = $('#dateRange').val();
  var year = $('#year').val();
  url += '?dateRange='+dateRange;
  if (dateRange == "single-year") { url += '&year='+year; }
  if ($('#useMainCategories').is(':checked')) {
    url += '&useMainCategories=1';
  }
  window.location = url;
}


function sortExpenses(a,b) {
  if (parseFloat(a.data) < parseFloat(b.data)) return 1;
  if (parseFloat(a.data) > parseFloat(b.data)) return -1;
  return 0;    
}


function plotExpenseChart() {   
  console.log("plotExpenseChart()");
  console.log(window.expenseData);
  var options = {
    grid: {
      hoverable: true,
      clickable: true 
    },
    series: {
      pie: { show: true }
    }
  };

  window.expenseData.sort(sortExpenses);

  $.plot($("#fullScreenChart"), window.expenseData, options);
}