var selectedTimePeriod = '';

function updateDateRangeFrom() {
  var from = $('#from');
  var to = $('#to');
  to.datepicker('option', 'minDate', from.val());
  updateDateRange();
}

function updateDateRangeTo() {    
  updateDateRange();
}


function updateTimePeriod(id) {
  if (id == selectedTimePeriod) { return; }

  var now = new Date();
  var thisYear = now.getFullYear();
  var lastYear = thisYear - 1;
  var thisDate = pad((now.getMonth()+1),2) + '-' + pad(now.getDate(),2);
  var today = thisYear + "-" + thisDate;
  console.log(today);
  if (selectedTimePeriod != '') {
    $('#'+selectedTimePeriod).toggleClass('selected');
  }
  $('#'+id).toggleClass('selected');
  selectedTimePeriod = id;

  if (id == "thisYear") {
    $('#from').val(thisYear + "-01-01");
    $('#to').val(today);
  }
  else if (id == "last12Mo") {
    $('#from').val((thisYear-1) + '-' + thisDate);
    $('#to').val(today);    
  }
  else if (id == "lastYear") {
    $('#from').val(lastYear + "-01-01");
    $('#to').val(lastYear + "-12-31");
  }
  else if (id == "last3") {
    $('#from').val((thisYear-3) + '-' + thisDate);
    $('#to').val(today);
  }
  else if (id == "last5") {
    $('#from').val((thisYear-5)+ '-' + thisDate);
    $('#to').val(today);
  }
  else if (id == "all") {      
    $('#from').val(window.startingDate);
    $('#to').val(today);
  }
  redrawChart();
}



function pad(n, width, z) {   
  z = z || '0';
  n = n + '';
  return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}


function buildTicks(chartData, maxTickCount) {    
  var ticks = [];
  var length = chartData.length;
  if (length <= maxTickCount) { 
    for (var i = 0; i < chartData.length; i++) { ticks.push([i, chartData[i][0]]); }
  }
  if (length > maxTickCount) { 
    var modulo = Math.floor(chartData.length / maxTickCount);

    // always include the first item
    ticks.push([0, chartData[0][0]]);

    for (var i = 1; i < chartData.length; i++) {
      if ((i % modulo) == 0) {
        ticks.push([i, chartData[i][0]]); }
    }
  }
  return ticks;
}


function calculateMaxTickCount(width) {
  if (width < 800) { return 6; }
  else if ((width > 800) && (width < 1200)) { return 8; }
  else { return 10; }
}


function formatTimeLabel(ts) {   
  var d = new Date(parseInt(ts));
  var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
  var month = months[d.getMonth()];
  return month + ' ' + d.getDate() + ', ' + d.getFullYear();
}