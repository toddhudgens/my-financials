$(document).ready(function() { 
  plotAutoTco(); 

  $("#carSelector").change(function() {
    window.location = '/automobile/tco?id='+this.value;
  });

});
$(window).resize(function() { plotAutoTco(); });



function plotAutoTco() {
  $.plot($("#fullScreenChart"), window.chartData, { 
    series: { 
      pie: { show: true }
    },
    grid: { hoverable: true, clickable: true }
  });

  $('#fullScreenChart').bind("plotclick", function(event, pos, obj) {

    if (!obj) { return; }
    percent = parseFloat(obj.series.percent).toFixed(2);
    var labels = $('td.legendLabel');
    for (var i = 0; i < labels.length; i++) {
      labels[i].style.textDecoration = 'none';
    }
    for (var i = 0; i < labels.length; i++) {
      if (obj.series.label == labels[i].innerText) { 
        labels[i].style.textDecoration = 'underline';
      }
    }
  });

}