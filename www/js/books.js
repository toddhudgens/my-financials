$(document).ready(function() {

    $('body').on("categoryUpdated", function (event) {});

    $('body').on("addAssetFormShown", function(event) {
	hideBookRows();
    });

    $('body').on("editAssetFormShown", function (event) {
	updateBookRowVis(event.categoryId);
	if (event.categoryId == 26) { populateBookInfo(event.assetId); }
    });
});


function hideBookRows() {
  $('.bookAssetInfo').hide();
}



function updateBookRowVis(catId) {
    if (catId == 26) {  $('.bookAssetInfo').show(); }
    else { $('.bookAssetInfo').hide(); }
}


function populateBookInfo(assetId) {

}
