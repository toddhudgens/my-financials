<?php

namespace ToddHudgens\MyFinancials\Model;

use ToddHudgens\MyFinancials\Model\Category as Category;

use PDO;

class Asset {


  public static function get($id) { 
    $dbh = dbHandle(1);
    $stmt = $dbh->prepare('SELECT a.* FROM assets a WHERE a.id=?');
    $stmt->execute(array($id));
    $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
    if ($results) {
      foreach ($results as $row) { return $row; }
    }
    else { return null; }
  }



  public static function getAll($showSold=0, $groupByCategory) {
    try {
      if ($showSold) { $sold = 1; } else { $sold = 0; }

      if ($groupByCategory) { $orderBy = 'c.name,a.name'; }
      else { $orderBy = 'a.name'; }

      $dbh = dbHandle(1);
      $q = 'SELECT a.id, a.name, a.initialValue, a.currentValue,
              a.notes, a.sold, a.dateSold,
              c.name as category, a.countryId,
              c.id as categoryId, cn.name as countryName,
              GROUP_CONCAT(p.filename) as pictures
            FROM assets a
            LEFT JOIN categories c ON a.categoryId=c.id
            LEFT JOIN countries cn ON a.countryId=cn.id
            LEFT JOIN pictures p ON a.id=p.entityId AND p.type="asset"
            WHERE a.sold='.$sold.'
            GROUP BY a.id
            ORDER BY '.$orderBy;
      //echo $q;
      $results = $dbh->query($q);
      $assets = $results->fetchAll(PDO::FETCH_ASSOC);
      if (!$groupByCategory) { return $assets; }

      $assetsByCategory = array();
      foreach ($assets as $i => $assetInfo) {
        $cName = $assetInfo['category'];
        foreach ($assetInfo as $key => $value) {
          if ($value == null) {
              //$assetInfo[$key] = '';
              unset($assetInfo[$key]);
          }
        }
        $ints = array('id','countryId','categoryId','sold');
        foreach ($ints as $key) {
          if (isset($assetInfo[$key])) {
            $assetInfo[$key] = intval($assetInfo[$key]);
          }
        }
        $assetInfo['initialValue'] = floatval($assetInfo['initialValue']);
        $assetInfo['currentValue'] = floatval($assetInfo['currentValue']);
        if (isset($assetInfo['countryName'])) { 
          if ($assetInfo['countryName'] == "United States of America") {
            $assetInfo['countryName'] = "USA";
          }
        }
        $assetsByCategory[$cName][] = $assetInfo;
      }
      return $assetsByCategory;
    }
    catch (PDOException $e) {}
  }



  public static function search($searchText) {
    $dbh = dbHandle(1);
    $q = 'SELECT a.id, a.name FROM assets a
          WHERE a.name LIKE ? ORDER BY a.name LIMIT 6';
    $stmt = $dbh->prepare($q);
    $stmt->execute(array('%'.$searchText.'%'));
    $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $response = array();
    if (count($results)) { 
      foreach ($results as $row) {
	$response[] = array('id' => $row['id'], 'name' => $row['name']);
      }
      return $response;
    }
    else { return array('no results'); }
  }


  public static function add() {
    if ($_GET['category'] == "") { $categoryId = Category::add($_GET['categoryName']); }
    else { $categoryId = $_GET['category']; }

    if (!isset($_GET['datePurchased']) || $_GET['datePurchased'] == "") { $datePurchased = null; }
    else { $datePurchased = $_GET['datePurchased']; }

    if (!isset($_GET['dateSold']) || ($_GET['dateSold'] == "")) { $dateSold = null; }
    else { $dateSold = $_GET['dateSold']; }

    try { 
      $dbh = dbHandle(1);
      $q = 'INSERT INTO assets '.
           '(id,name,categoryId,initialValue,currentValue,liquid,notes,countryId,sold,datePurchased,dateSold) '.
           'VALUES '.
           '(0, :name,:categoryId,:purchasePrice,:currentValue,1,:notes,:madeIn,:sold,'.
	   ':datePurchased,:dateSold)';
      $stmt = $dbh->prepare($q);
      $stmt->bindParam(':name', $_GET['name']);
      $stmt->bindParam(':categoryId', $categoryId);
      $stmt->bindParam(':purchasePrice', $_GET['purchasePrice']);
      $stmt->bindParam(':currentValue', $_GET['currentValue']);
      $stmt->bindParam(':notes', $_GET['notes']);
      $stmt->bindParam(':madeIn', $_GET['madeIn']);
      $stmt->bindParam(':datePurchased', $datePurchased);
      $stmt->bindParam(':sold', $_GET['sold']);
      $stmt->bindParam(':dateSold', $dateSold);
      $stmt->execute();
      $assetId = $dbh->lastInsertId();
      return $assetId;
    }
    catch (PDOException $ex) { die($e->getMessage()); }
  }


  public static function update() {
    if ($_GET['category'] == "") { $categoryId = Category::add($_GET['categoryName']); }
    else { $categoryId = $_GET['category']; }

    if ($_GET['dateSold'] == "") { $dateSold = null; }  else { $dateSold = $_GET['dateSold']; }
    
    try { 
      $dbh = dbHandle(1);
      $q = 'UPDATE assets
            SET name=:name,
                categoryId=:categoryId,
                currentValue=:currentValue,
                initialValue=:purchasePrice,
                sold=:sold,
                dateSold=:dateSold,
                notes=:notes,
                countryId=:madeIn
            WHERE id=:id';
      $stmt = $dbh->prepare($q);
      $stmt->bindParam(':name', $_GET['name']);
      $stmt->bindParam(':categoryId', $categoryId);
      $stmt->bindParam(':currentValue', $_GET['currentValue']);
      $stmt->bindParam(':purchasePrice', $_GET['purchasePrice']);
      $stmt->bindParam(':notes', $_GET['notes']);
      $stmt->bindParam(':madeIn', $_GET['madeIn']);
      $stmt->bindParam(':sold', $_GET['sold']);
      $stmt->bindParam(':dateSold', $dateSold);
      $stmt->bindParam(':id', $_GET['assetId']);
      $stmt->execute();
      if ($stmt->rowCount()) { return 1; }
    }
    catch (PDOException $ex) {
      return 0;
      //$e->getMessage();
    }
  }


  public static function updateCurrentValue($assetId, $value) {
    $dbh = dbHandle(1);
    $q = 'UPDATE assets SET currentValue=:value WHERE id=:assetId';
    $stmt = $dbh->prepare($q);
    $stmt->bindParam(':value', $value);
    $stmt->bindParam(':assetId', $assetId);
    $stmt->execute();
  }


  public static function delete($id) {
    try {
      $dbh = dbHandle(1);
      $stmt = $dbh->prepare('DELETE FROM assets WHERE id=?');
      $stmt->execute(array($_GET['id']));
      if ($stmt->rowCount()) { return array('success'); }
      else { returnarray('error', 'nothing to delete'); }
    }
    catch (PDOException $e) {return array('error', $e->getMessage()); }
  }


  public static function totalValue() {
    try {
      $dbh = dbHandle(1);
      $results = $dbh->query('SELECT SUM(currentValue) as assetValue FROM assets WHERE sold=0');
      foreach ($results as $row) { return $row['assetValue']; }
    }
    catch (PDOException $e) {}
    
    return 0;
  }


  public static function valuesByCategory($liquid='') {
    $where = ''; 
    if ($liquid == 1) { $where = ' AND a.liquid=1'; }

    $dbh = dbHandle(1);
    $q = 'SELECT GROUP_CONCAT(a.id) as assetIds, c.name as category,SUM(a.currentValue) as totalValue
          FROM assets a, categories c
          WHERE a.sold=0 AND a.categoryId=c.id '. $where .'
          GROUP BY c.id
           ORDER BY totalValue DESC';
    $stmt = $dbh->prepare($q);
    $stmt->execute();
    $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
    if ($results) { 
      foreach ($results as $row) {
        $row['label'] = Category::buildLabel($row);
        $row['assetIdArray'] = explode(',', $row['assetIds']);
        $assets[] = $row;
      }
    }

    // add accounts to asset allocation. if it's tied to a asset we can 
    // update the balance by removing the debt value
    $q = 'SELECT a.id, a.assetId, a.balance, e.name, SUM(sa.currentValue) as portfolioValue
          FROM accounts a 
          LEFT JOIN entities e ON a.entityId=e.id 
          LEFT JOIN stockAssets sa ON a.id=sa.accountId
          WHERE 1=1 ' . $where . ' GROUP BY a.id';
    $stmt = $dbh->prepare($q);
    $stmt->execute();
    $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
    foreach ($results as $row) {
        
      if ($row['assetId'] > 0) {
        foreach ($assets as $id => $assetInfo) {
          if (isset($assetInfo['assetIdArray']) && is_array($assetInfo['assetIdArray'])) { 
            if (in_array($row['assetId'], $assetInfo['assetIdArray'])) { 
              $assets[$id]['totalValue'] += $row['balance'];
            }
          }
        }
      }
      else {
        $row['totalValue'] = $row['balance'];
        if ($row['portfolioValue'] != NULL) { $row['totalValue'] += $row['portfolioValue']; }
        $row['label'] = $row['name'];
        if ($row['totalValue'] > 0) { 
          $assets[] = $row;
        }
      }
    }

    // sort by value
    $value = array(); 
    foreach ($assets as $key => $row) { $value[$key] = $row['totalValue']; }
    array_multisort($value, SORT_DESC, $assets);

    return $assets;
  }


  public static function getForAutomobileOverview($catId) { 
    $dbh = dbHandle(1);
    $q = 'SELECT a.*,act.id as accountId, GROUP_CONCAT(p.filename) as pictures '.
         'FROM assets a '.
         'LEFT JOIN accounts act ON a.id=act.assetId '.
         'LEFT JOIN pictures p ON a.id=p.entityId AND p.type="asset" '.
         'WHERE a.categoryId=? '.
         'GROUP BY a.id '.
         'ORDER BY a.sold ASC, a.dateSold DESC';
    $stmt = $dbh->prepare($q);
    $stmt->execute(array($catId));
    $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $automobiles = array();
    foreach ($results as $row) {
      $automobiles[$row['id']] = $row;
      $pictures = explode(',',$row['pictures']);
      $automobiles[$row['id']]['picture'] = '/images/assets/'.$pictures[0];
    }

    $q = 'SELECT assetId,count(*) as logEntries FROM vehicleMaintenance GROUP BY assetId';
    $stmt = $dbh->prepare($q);
    $stmt->execute();
    $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
    foreach ($results as $row) {
        $assetId = $row['assetId'];
        $automobiles[$assetId]['maintenanceLogEntries'] = $row['logEntries'];
    }

    $q = 'SELECT assetId,count(*) as gasMileageEntries FROM gasMileage GROUP BY assetId';
    $stmt = $dbh->prepare($q);
    $stmt->execute();
    $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
    foreach ($results as $row) {
        $assetId = $row['assetId'];
        $automobiles[$assetId]['gasLogEntries'] = $row['gasMileageEntries'];
    }

    $q = 'SELECT assetId,count(*) as taxEntries FROM vehicleTax GROUP BY assetId';
    $stmt = $dbh->prepare($q);
    $stmt->execute();
    $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
    foreach ($results as $row) {
        $assetId = $row['assetId'];
        $automobiles[$assetId]['taxLogEntries'] = $row['taxEntries'];
    }


    $q = 'SELECT assetId,count(*) as insuranceEntries FROM vehicleInsurance GROUP BY assetId';
    $stmt = $dbh->prepare($q);
    $stmt->execute();
    $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
    foreach ($results as $row) {
        $assetId = $row['assetId'];
        $automobiles[$assetId]['insuranceEntries'] = $row['insuranceEntries'];
    }
    
    
    return $automobiles;
  }

}




?>