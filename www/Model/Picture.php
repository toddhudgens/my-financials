<?php

namespace ToddHudgens\MyFinancials\Model;

use PDO;

class Picture {

  public static function uploadFiles($files, $entityType, $entityId) {
    $imageFolder = '/sites/fin.toddhudgens.com/www/images/' . $entityType . '/';
    $dbEntityType = substr($entityType,0,-1); // plural to singular 
    $dbh = dbHandle(1);

    $tmpFiles = $files['tmp_name'];

    $success = true;
    $addedImages = array();

    foreach ($tmpFiles as $i => $path) {
      $ext = self::extensionForMime($files['type'][$i]);
      $dimensions = getimagesize($path);

      $q = 'SHOW TABLE STATUS WHERE name="pictures"';
      $stmt = $dbh->prepare($q);
      $stmt->execute();
      $result = $stmt->fetch(PDO::FETCH_ASSOC);
      $nextId = $result['Auto_increment'];

      $destFileName = $nextId.$ext;
      move_uploaded_file($path, $imageFolder.$destFileName);
      $originalFileName = $files['name'][$i];
      $filesize = $files['size'][$i];

      $q = 'INSERT INTO pictures '.
             '(type,entityId,filename,uploadedFileName,width,height,filesize) VALUES '.
             '(:type,:entityId,:filename,:originalFileName,:width,:height,:filesize)';
      $stmt = $dbh->prepare($q);
      $stmt->bindParam(':type', $dbEntityType);
      $stmt->bindParam(':entityId', $entityId);
      $stmt->bindParam(':filename', $destFileName);
      $stmt->bindParam(':originalFileName', $originalFileName);
      $stmt->bindParam(':width', $dimensions[0]);
      $stmt->bindParam(':height', $dimensions[1]);
      $stmt->bindParam(':filesize', $filesize);
      $stmt->execute();
      if ($stmt->rowCount()) {
        $id = $dbh->lastInsertId();
        $addedImages[] = array('id' => $id,
                               'filename' => $destFileName,
                               'path' => '/images/'.$entityType.'/'.$destFileName);
      }
      else { $success = false; }
    }
    $response = array('result' => ($success ? 'success': 'failure'), 'addedImages' => $addedImages);
    return $response;
  }


  public static function delete($id) {
    $imageFolder = '/sites/fin.toddhudgens.com/www/images/';

    $dbh = dbHandle(1);
    $q = 'SELECT * FROM pictures WHERE id=:id';
    $stmt = $dbh->prepare($q);
    $stmt->bindParam(':id', $id);
    $stmt->execute();
    $row = $stmt->fetch(PDO::FETCH_ASSOC);
    
    if ($row) {
    
      $path = $imageFolder . $row['type'].'s/' . $row['filename'];
      unlink($path);

      $q = 'DELETE FROM pictures WHERE id=:id';
      $stmt = $dbh->prepare($q);
      $stmt->bindParam(':id', $id);
      $stmt->execute();
      return array('result' => "success");
    }
    else { return array('result' => "failure"); }
  }


  public static function extensionForMime($mime) {
    if ($mime == "image/jpeg") { return ".jpg"; }
    else if ($mime == "image/gif") { return ".gif"; }
    else if ($mime == "image/png") { return ".png"; }
  }
}

?>