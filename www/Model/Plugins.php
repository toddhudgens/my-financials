<?php

namespace ToddHudgens\MyFinancials\Model;

use ReflectionMethod;

class Plugins {

  public static function run($methodName, $params) {
    global $plugins; 
    foreach ($plugins as $p => $pluginScript) {
      //die($pluginScript . ', ' . $methodName);
      $method = new ReflectionMethod("\\ToddHudgens\\MyFinancials\\Model\\".$pluginScript, $methodName);
      //$method = new ReflectionMethod($pluginScript, $methodName);
      echo $method->invokeArgs(null, $params);
    }
  }
}

?>