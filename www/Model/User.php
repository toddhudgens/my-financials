<?php

namespace ToddHudgens\MyFinancials\Model;

use PDO;

class User {

public static function login($un, $pw) {
  $salt = '01623e2813c32fbbb63cd0e7881cd50b';
  $dbh = dbHandle(0);
  $stmt = $dbh->prepare('SELECT id,password FROM users WHERE (name = :un)');
  $stmt->bindValue(':un', $un);
  $stmt->execute();
  $result = $stmt->fetch(PDO::FETCH_ASSOC);
  $isValid = password_verify($pw.$salt, $result['password']);
  if ($isValid) {
    $_SESSION['valid'] = 1;
    $_SESSION['name'] = $_POST['username'];
    $_SESSION['userId'] = $result['id'];
  }
  else {
    $_SESSION['valid'] = null;
    $_SESSION['userId'] = null;
  }
  return $isValid;
}



public static function add($params) {
  $salt = '01623e2813c32fbbb63cd0e7881cd50b';
  $pw = password_hash($params['password1'].$salt, PASSWORD_DEFAULT);

  $dbh = dbHandle(0);
  $q = 'INSERT INTO users VALUES(0,:un,:pw)';
  $stmt = $dbh->prepare($q);
  $stmt->bindValue(':un', $params['username']);
  $stmt->bindValue(':pw', $pw);
  $stmt->execute();
  if ($stmt->rowCount()) { return true; }
  else { return false; }
}



public static function total() {
  $dbh = dbHandle(0);
  $q = 'SELECT count(*) as cnt FROM users';
  $stmt = $dbh->prepare($q);
  $stmt->execute();
  $result = $stmt->fetch(PDO::FETCH_ASSOC);
  return $result['cnt'];
}


}

?>