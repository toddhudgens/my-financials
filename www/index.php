<?php

date_default_timezone_set("America/New_York");

$GLOBALS['executionStart'] = microtime(true);
$GLOBALS['sessionTimeout'] = 900;
ini_set('session.gc_maxlifetime', $GLOBALS['sessionTimeout']);
session_start();
$dontUpdateTimeout = array('/user/session-check');


// update the session with every page load to modify the session data on disk
if (isset($_SESSION['LAST_ACTIVITY']) &&
   ((time() - $_SESSION['LAST_ACTIVITY']) > $GLOBALS['sessionTimeout'])) {
  session_unset();
  session_destroy();
}

// dont update the last activity timestamp for /user/session-check
if (!in_array($_SERVER['REQUEST_URI'], $dontUpdateTimeout)) {
  $_SESSION['LAST_ACTIVITY'] = time();
}

require_once('common/autoloading.php');
require_once('vendor/autoload.php');
require_once('common/db.php');
require_once('common/util.php');
require_once('common/pluginLoader.php');

// initialize view variables
$GLOBALS['view'] = array();
$GLOBALS['view']['css'] = array();
$GLOBALS['view']['js'] = array();
$GLOBALS['view']['title'] = '';
$GLOBALS['view']['year'] = date('Y');
$GLOBALS['view']['loggedIn'] = isLoggedIn();

$loader = new Twig_Loader_Filesystem('templates');
$twig = new Twig_Environment($loader,
			     array('cache' => '/tmp/finances/cache/'));
$GLOBALS['twig'] = $twig;


// Create and configure Slim app
$config = ['settings' => ['addContentLengthHeader' => false]];
$app = new \Slim\App($config);

//$container = $app->getContainer();
//$container['view'] = function ($container) { $view = new \Slim\Views\Twig('templates', ['cache' => '../cache/']); };


// Define app routes
$ns = '\\ToddHudgens\\MyFinancials\\Controller\\';

if (!loggedIn()) {
  $app->get('/user/login', $ns.'UserController:login');
  $app->get('/user/logout', $ns.'UserController:logout');
  $app->post('/user/login-submit', $ns.'UserController:loginSubmit');
  $app->get('/user/session-check', $ns.'UserController:sessionCheck');
  $app->post('/user/init-add-submit', $ns.'UserController:initAddSubmit');
  $app->any('/', $ns.'UserController:login');
  $app->get('/update-nw', $ns.'NetworthController:update');
  $app->get('/update-stock-prices', $ns.'StocksController:updatePrices');
  $app->get('/update-pm-prices', $ns.'PreciousMetalController:updatePrices');
  $app->get('/{request-uri}', $ns.'UserController:login');
}
else {
  loadPlugins(); 
  $app->get('/', $ns.'UserController:index');
  $app->get('/user/logout', $ns.'UserController:logout');
  $app->get('/user/session-check', $ns.'UserController:sessionCheck');
  $app->get('/user/session-update', $ns.'UserController:sessionUpdate');

  $app->get('/account/assets', $ns.'AccountController:assets');
  $app->get('/account/save', $ns.'AccountController:save');
  $app->get('/account/show-transactions', $ns.'AccountController:showTransactions');

  $app->post('/transaction/save', $ns.'TransactionController:save');
  $app->get('/transaction/delete', $ns.'TransactionController:delete');
  $app->get('/transaction/search', $ns.'TransactionController:search');
  $app->post('/transaction/picture-upload', $ns.'PictureController:uploadTransactionImages');

  $app->get('/assets', $ns.'AssetController:listAll');
  $app->get('/sold-assets', $ns.'AssetController:listSold');
  $app->get('/asset/save', $ns.'AssetController:save');
  $app->get('/asset/delete', $ns.'AssetController:delete');
  $app->get('/asset/search', $ns.'AssetController:search');
  $app->post('/asset/picture-upload', $ns.'PictureController:uploadAssetImages');
  $app->get('/picture-delete', $ns.'PictureController:delete');

  $app->get('/mortgage-calculator', $ns.'MortgageCalculatorController:index');

  $app->get('/report/asset-allocation', $ns.'ReportController:assetAllocation');
  $app->get('/report/expenses', $ns.'ReportController:expenses');
  $app->get('/report/gas-prices', $ns.'ReportController:gasPrices');
  $app->get('/report/liquid-asset-allocation', $ns.'ReportController:liquidAssetAllocation');
  $app->get('/report/expenses-by-category', $ns.'ReportController:expensesByCategory');
  $app->get('/report/expenses-by-entity', $ns.'ReportController:expensesByEntity');
  $app->get('/report/expenses-by-tag', $ns.'ReportController:expensesByTag');
  $app->get('/report/networth', $ns.'ReportController:networth');
  $app->get('/report/yearly-summary', $ns.'ReportController:yearlySummary');

  $app->get('/monthly-budget', $ns.'MonthlyBudgetController:index');
  $app->get('/monthly-budget/save-item', $ns.'MonthlyBudgetController:saveItem');
  $app->get('/monthly-budget/delete-item', $ns.'MonthlyBudgetController:deleteItem');
  $app->get('/automobiles', $ns.'AutomobileController:index');

  $app->get('/automobile/maintenance', $ns.'AutomobileController:maintenance');
  $app->get('/automobile/gas-mileage', $ns.'AutomobileController:gasMileage');
  $app->get('/automobile/tco', $ns.'AutomobileController:tco');
  $app->get('/automobile/log', $ns.'AutomobileController:fullLog');
  $app->get('/automobile/maintenance-info', $ns.'AutomobileController:maintenanceInfo');
  $app->get('/automobile/gas-mileage-info', $ns.'AutomobileController:gasMileageInfo');
  $app->get('/automobile/insurance-info', $ns.'AutomobileController:insuranceInfo');
  $app->get('/automobile/tax-info', $ns.'AutomobileController:taxInfo');
  $app->post('/automobile/update-maintenance-notes', $ns.'AutomobileController:updateMaintenanceNotes');

  $app->get('/categories', $ns.'CategoryController:listAll');

  $app->get('/category/save', $ns.'CategoryController:save');
  $app->get('/category/delete', $ns.'CategoryController:delete');
  $app->get('/category/search', $ns.'CategoryController:search');

  $app->get('/entities', $ns.'EntityController:listAll');

  $app->get('/entity/save', $ns.'EntityController:save');
  $app->get('/entity/delete', $ns.'EntityController:delete');
  $app->get('/entity/search', $ns.'EntityController:search');

  $app->get('/tags', $ns.'TagController:listAll');

  $app->get('/tag/save', $ns.'TagController:save');
  $app->get('/tag/delete', $ns.'TagController:delete');
  $app->get('/tag/search', $ns.'TagController:search');

  $app->get('/update-nw', $ns.'NetworthController:update');
  $app->get('/update-stock-prices', $ns.'StocksController:updatePrices');
  $app->get('/update-stock-assets', $ns.'StocksController:updateAssets');
  $app->get('/stock-price-entry', $ns.'StocksController:manualPriceUpdate');
  $app->post('/stock-price-update', $ns.'StocksController:manualPriceSubmit');
  $app->get('/precious-metal-asset-info', $ns.'PreciousMetalController:assetInfo');
  $app->get('/update-pm-prices', $ns.'PreciousMetalController:updatePrices');
}

/*
echo '<pre>';
print_r($app);
echo '</pre>';
*/

$app->run();

?>
