<?php

function loadPlugins() {
  global $plugins;
  $dbh = dbHandle(1);
  $results = $dbh->query("SELECT * FROM plugins");
  foreach ($results as $plugin) {
    if ($plugin['enabled'] == 1) { 
      $plugins[$plugin['script']] = $plugin['classname'];      
      if (file_exists(getcwd().'/css/'.$plugin['script'].'.css')) { addCSS($plugin['script'].'.css'); }
      if (file_exists(getcwd().'/js/'.$plugin['script'].'.js')) { addJS($plugin['script'].'.js'); }
    }
  }
  //print_r($css);
}


?>