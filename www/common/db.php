<?php

function dbHandle() {
  if (isset($GLOBALS['dbh'])) { return $GLOBALS['dbh']; }
  else { 
    $GLOBALS['dbh'] = new PDO('mysql:host=localhost;dbname=finances', 'finances_admin', getenv('DBPASSWORD'));
    return $GLOBALS['dbh'];
  }
}

?>