<?php


function addCSS($script) {
  global $view;
  if ($script == null) { return; }
  if (is_array($script)) { 
    foreach ($script as $i => $scr) { 
      $view['css'][] = $scr;
    }
  }
  else { $view['css'][] = $script; }
}

function addJS($script) {
  global $view;
  if ($script == null) { return; }
  if (is_array($script)) { 
    foreach ($script as $i => $scr) { 
      $view['js'][] = $scr;
    }
  }
  else { $view['js'][] = $script; }
}

function redirectToPage($page, $timer) { 
  print '<meta http-equiv="refresh" target="_top" content="' . $timer . ';url=' . $page . '">'; 
}

function isLoggedIn() { return loggedIn(); }

function loggedIn() { if (isset($_SESSION['userId'])) { return true; }  else { return false; } }

?>