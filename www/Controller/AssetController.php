<?php

namespace ToddHudgens\MyFinancials\Controller;

use ToddHudgens\MyFinancials\Model\Asset as Asset;
use ToddHudgens\MyFinancials\Model\Country as Country;
use ToddHudgens\MyFinancials\Model\Plugins as Plugins;
use ToddHudgens\MyFinancials\Model\Twig as Twig;


class AssetController { 

function delete() {
  $response = Asset::delete($_GET['id']);
  echo json_encode($response);
}


function search() {
  $response = Asset::search($_GET['q']);
  header('Content-Type: text/plain');
  echo json_encode($response);
}



function save() {
  $response = array('success');

  try { 
    if ($_GET['mode'] == "edit") {
      if (Asset::update()) { 
        Plugins::run('assetUpdate', array($_REQUEST['assetId']));
      }
      else {
        //$response = array('error', 'no rows updated');
      }
    }
    else if ($_GET['mode'] == "new") {
      $assetId = Asset::add();
      if ($assetId) { 
        Plugins::run('assetCreate', array($_REQUEST['assetId']));
      }
    }
  }
  catch (PDOException $e) { $response = array('error', $e->getMessage()); }
  echo json_encode($response);
}



function listAll() {

  $groupByCategory = 1;
  $template = 'assets-listing.twig';
  if (isset($_GET['groupByCategory']) && ($_GET['groupByCategory'] == 0)) {
    $groupByCategory = 0;
    $template = 'assets-listing-ungrouped.twig';
  }

  $assets = Asset::getAll(0, $groupByCategory);
  $categories = array();
  $assetEditFields = ''; 
  
  Plugins::run('assetListingCustomizations', array(&$assets));
  Plugins::run('assetCategoryExtraInfo', array(&$categories));
  Plugins::run('assetEditFields', array(&$assetEditFields));
  $countries = Country::getAll();

  Twig::render($template,
               array('assets' => $assets,
                     'assetEditFields' => $assetEditFields,
                     'categoryDetails' => $categories,
                     'countries' => $countries,
                     'groupByCategory' => $groupByCategory,
                     'title' => 'Asset Listing'));
}


function listSold() {
  $assets = Asset::getAll(1,1);
  $categories = array();
  $assetEditFields = '';

  Plugins::run('assetListingCustomizations', array(&$assets));
  Plugins::run('assetCategoryExtraInfo', array(&$categories));
  Plugins::run('assetEditFields', array(&$assetEditFields));
  $countries = Country::getAll();

  Twig::render('assets-sold-listing.twig',
    array('assets' => $assets,
          'assetEditFields' => $assetEditFields,
          'categoryDetails' => $categories,
          'countries' => $countries,
          'title' => 'Asset Listing'));
}




}

?>