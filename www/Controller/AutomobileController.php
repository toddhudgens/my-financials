<?php

namespace ToddHudgens\MyFinancials\Controller;

use ToddHudgens\MyFinancials\Model\Asset as Asset;
use ToddHudgens\MyFinancials\Model\Automobile as Automobile;
use ToddHudgens\MyFinancials\Model\Twig as Twig;

class AutomobileController { 

function maintenanceInfo() {
  $info = Automobile::getMaintenanceInfo($_REQUEST['transactionId']);
  echo json_encode($info);
}


function gasMileageInfo() { 
  $info = Automobile::getGasMileageInfo($_REQUEST['transactionId']);
  echo json_encode($info);
}


function taxInfo() {
  $info = Automobile::getTaxInfo($_REQUEST['transactionId']);
  echo json_encode($info);
}


function insuranceInfo() {
  $info = Automobile::getInsuranceInfo($_REQUEST['transactionId']);
  echo json_encode($info);
}


function updateMaintenanceNotes() {
  $res = Automobile::updateMaintenanceNotes($_POST['id'], $_POST['notes']);
  echo $res;
}


function index() {
  $catId = Automobile::getCategoryId();
  $assets = Asset::getForAutomobileOverview($catId);
  Twig::render('automobile-overview.twig',
	       array('assets' => $assets,
                     'title' => 'My Automobiles'));
}


function gasMileage() {
  global $title; 
  $vehicleInfo = Asset::get($_GET['id']);
  $title = $vehicleInfo['name'] . ' - Gas Mileage Report';
  $mileageInfo = Automobile::getGasMileage($_GET['id']);
  $lastReading = 0;
  $lastTank = null;
  $totals = array('milesDriven' => 0, 'gasPumped' => 0, 'totalSpent' => 0);

  foreach ($mileageInfo as $i => $log) {
    if (isset($log['milesDriven']) && ($log['milesDriven'] > 0)) {
      $milesDriven = $log['milesDriven'];
    }
    else { 
      if ($i > 0) { 
        if ($log['mileage'] != '') { $milesDriven = $log['mileage'] - $lastReading; }
        else { $milesDriven = null; }
      }
    }

    $mileageInfo[$i]['amount'] = abs($log['amount']);
    $totals['gasPumped'] += $log['gasPumped'];
    $totals['totalSpent'] += abs($log['amount']);
    if (isset($milesDriven) && isset($log['gasPumped'])) { 
      $mileageInfo[$i]['milesDriven'] = $milesDriven;
      $totals['milesDriven'] += $milesDriven;
      $mileageInfo[$i]['mpg'] = round($milesDriven / $log['gasPumped'],1);
    }
    else {
      if (isset($milesDriven)) { $mileageInfo[$i]['milesDriven'] = $milesDriven; }
      else { $mileageInfo[$i]['milesDriven'] = '--'; }
      $mileageInfo[$i]['mpg'] = '--';
    }
    if ($log['mileage'] != "") {
      $lastReading = $log['mileage'];
    }
    $lastTank = $log['gasPumped'];
  }

  if ($totals['gasPumped'] > 0) { 
    $totals['averageMPG'] = round($totals['milesDriven'] / $totals['gasPumped'], 1);
  }
  else { $totals['averageMPG'] = 0; }
  
  if ($totals['milesDriven'] > 0) {
    $totals['costPerMile'] = round(($totals['totalSpent'] / $totals['milesDriven'] * 100), 1);
  }
  else { $totals['costPerMile'] = 0; }

  Twig::render('automobile-mileage-log.twig',
	       array('id' => $_GET['id'],
		     'automobiles' => Automobile::getAll(),
                     'title' => $title,
                     'info' => $vehicleInfo,
                     'mileageInfo' => $mileageInfo,
                     'totals' => $totals));
}



// vehicle maintenance report
function maintenance() { 
  global $title;
  //die($_GET['view']);
  $vehicleInfo = Automobile::getInfo($_GET['id']);
  if (isset($_GET['view']) && ($_GET['view'] == "log")) { 
    $view = $_GET['view']; 
    $viewScript = 'automobile-log.twig';
    $title = $vehicleInfo['name'] . ' Log';
  } 
  else { 
    $view = 'maintenance'; 
    $viewScript = 'automobile-maintenance-log.twig';
    $title = $vehicleInfo['name'] . ' Service Log';
  }
  $maintenanceLog = Automobile::getVehicleLog($_GET['id'], $view);

  $totals = array('totalSpent' => 0, 'maintenanceTotal' => 0, 'upgradeTotal' => 0, 'milesLogged' => 0);

  if (isset($vehicleInfo['startingOdometer'])) {
    $lowestOdo = $vehicleInfo['startingOdometer'];
    $lowSet = true;   
  }
  else {
    $lowestOdo = 999999999;
    $lowSet = false;
  }
  $highestOdo = -1;
  foreach ($maintenanceLog as $i => $log) { 
    $totals['totalSpent'] += abs($log['amount']);

    if ($log['upgradeWork'] == 1) { $totals['upgradeTotal'] += $log['amount']; }
    else { $totals['maintenanceTotal'] += $log['amount']; }
    
    if ($log['mileage'] != '-') { 
      if (!$lowSet && ($log['mileage'] < $lowestOdo)) { $lowestOdo = $log['mileage']; $lowSet = true; }
      if ($log['mileage'] > $highestOdo) { $highestOdo = $log['mileage']; }
    }
    $maintenanceLog[$i]['amount'] = abs($log['amount']);
  }

  $latestMileage = Automobile::getLatestMileage($_GET['id']);
  if ($latestMileage > $highestOdo) { $highestOdo = $latestMileage; }
  
  $totals['milesLogged'] = ($highestOdo - $lowestOdo);
  if ($totals['milesLogged'] > 0) { 
    $totals['costPerMile'] = ($totals['totalSpent'] / $totals['milesLogged']);
  }
  else { $totals['costPerMile'] = '0'; }

  Twig::render($viewScript,
               array('id' => $_GET['id'],
                     'view' => $view, 
                     'automobiles' => Automobile::getAll(),
                     'title' => $title,
                     'info' => $vehicleInfo,
                     'maintenanceLog' => $maintenanceLog,
                     'totals' => $totals));
}



function fullLog() {
  $_GET['view'] = 'log';
  self::maintenance();
}



function tco() {
  if (!isset($_GET['id'])) { exit; }
  $id = $_GET['id'];
  addCSS('report.css');
  $vehicleInfo = Automobile::getInfo($id);
  global $title;
  $title = $vehicleInfo['name'] . ' - TCO Report';
  $totals = array();
  $totals['start'] = $vehicleInfo['startingOdometer']; 
  $totals['end'] = Automobile::getLatestMileage($id);
  $totals['milesDriven'] = $totals['end'] - $totals['start'];
  $tco = Automobile::getTCO($id);

  if ($tco['depreciation'] < 0) { $tco['depreciation'] = 0; }
  if ($totals['milesDriven'] > 0) { 
    $totals['costPerMile'] = round(($tco['totalCost'] / $totals['milesDriven'] * 100), 1);
  }
  else { $totals['costPerMile'] = 0; }

  if ($vehicleInfo['datePurchased'] == "") { 
    if ($vehicleInfo['firstTransaction'] != "") {
      $secondsOwned = time() - strtotime($vehicleInfo['firstTransaction']);
    }
    else { $secondsOwned = null; }
  }
  else {
    if ($vehicleInfo['dateSold'] == "") { 
      $secondsOwned = time() - strtotime($vehicleInfo['datePurchased']);
    }
    else {
      $secondsOwned = strtotime($vehicleInfo['dateSold']) -
                   strtotime($vehicleInfo['datePurchased']);
    }
  }

  if ($secondsOwned != null) {
    $daysOwned = ($secondsOwned / (60*60*24));
    $totals['costPerMonth'] = $tco['totalCost'] / ($daysOwned / 30.4167);
  }

  $metrics = array('depreciation', 'interest', 'fuel', 'insurance', 'maintenance', 'upgrades', 'taxes');
  $colors = array('#cb4b4b', '#9440ed', '#333', '#4da74d', '#afd8f8', '#0099cc', '#edc240');
  $chartData = array();
  for ($i = 0; $i < count($metrics); $i++) { 
    $key = $metrics[$i];
    $label = ucwords($key) . ' ($'.$tco[$key].')';
    $chartData[] = array('label' => $label, 'data' => $tco[$key], 'color' => $colors[$i]);
  }

  Twig::render('automobile-tco-report.twig',
               array('id' => $_GET['id'],
                     'vehicleInfo' => $vehicleInfo,
                     'automobiles' => Automobile::getAll(),
                     'title' => $title,
                     'chartData' => $chartData,
                     'tco' => $tco,
                     'totals' => $totals));
}
}

?>