<?php

namespace ToddHudgens\MyFinancials\Controller;

use ToddHudgens\MyFinancials\Model\Stocks as Stocks;
use ToddHudgens\MyFinancials\Model\Twig as Twig;
use PDO;

class StocksController { 

function updatePrices() {
  $dbh = dbHandle();

  // get all of the unique stocks we need to track
  $q = 'SELECT distinct ticker FROM stockAssets WHERE qty>0 ORDER BY priceUpdated ASC';
  $results = $dbh->query($q);
  $stockTickers = $results->fetchAll(PDO::FETCH_ASSOC);
  //print_r($stockTickers);

  // update their prices
  foreach ($stockTickers as $i => $info) {
    echo "Updating price for: " . $info['ticker'];
    $response = Stocks::updatePrice($info['ticker']);
    if ($response) { echo " (success)<br>"; }
    else { echo " (failure)<br>"; }
  }

  // update the stockAssets table
  foreach ($stockTickers as $i => $info) { 
    $price = Stocks::getLatestPrice($info['ticker']);
    Stocks::updateAssetsWithTicker($info['ticker'], $price);
  }
}


function updateAssets() {
  // get all of the unique stocks we need to track
  $dbh = dbHandle();
  $q = 'SELECT distinct ticker FROM stockAssets WHERE qty<>0';
  $results = $dbh->query($q);
  $stockTickers = $results->fetchAll(PDO::FETCH_ASSOC);
  //print_r($stockTickers);

  foreach ($stockTickers as $i => $info) {
    $price = Stocks::getLatestPrice($info['ticker']);
    Stocks::updateAssetsWithTicker($info['ticker'], $price);
  }
}

function manualPriceUpdate() {
  $dbh = dbHandle();
  $q = 'SELECT *,(currentValue/qty) as lastPrice FROM stockAssets WHERE qty>0 '.
       'GROUP BY ticker ORDER BY ticker';
  $results = $dbh->query($q);
  $stocksOwned = $results->fetchAll(PDO::FETCH_ASSOC);
  $viewParams = array('title' => 'Stock Price Manual Entry',
                      'stocks' => $stocksOwned);
  //echo '<pre>'; print_r($stocksOwned); echo '</pre>';
  Twig::render('stock-price-entry.twig', $viewParams);
}

function manualPriceSubmit() {
  $postBody = file_get_contents('php://input');
  $stockPrices = json_decode($postBody);
  for ($i = 0; $i < count($stockPrices); $i++) {
    $ticker = $stockPrices[$i]->ticker;
    $price = $stockPrices[$i]->price;
    if ($price != '') {
      echo $ticker . ', ' . $price;
      Stocks::insertPrice($ticker, $price);
      Stocks::updateAssetsWithTicker($ticker, $price);
    }
  }
}

}

?>