<?php

namespace ToddHudgens\MyFinancials\Controller;

use ToddHudgens\MyFinancials\Model\MonthlyBudget as MonthlyBudget;
use ToddHudgens\MyFinancials\Model\Twig as Twig;

class MonthlyBudgetController { 

function index() {
  $incomeItems = MonthlyBudget::getIncome();
  $fixedExpenses = MonthlyBudget::getFixedExpenses();
  $variableExpenses = MonthlyBudget::getVariableExpenses();
  $retirement = MonthlyBudget::getRetirementExpenses();

  $combinedItems = array_merge($incomeItems, $fixedExpenses, $variableExpenses, $retirement);
  $itemsById = array();
  foreach ($combinedItems as $id => $info) { $itemsById[$info['id']] = $info; }

  $viewParams = array('title' => "Monthly Budget",
                      'income' => $incomeItems,
                      'fixedExpenses' => $fixedExpenses,
                      'variableExpenses' => $variableExpenses, 
                      'retirement' => $retirement,
                      'items' => $itemsById);
  Twig::render('monthly-budget.twig', $viewParams);
}


function saveItem() {
  $response = array('success');

  try {
    if ($_GET['itemId'] != "") { MonthlyBudget::updateItem(); }
    else { MonthlyBudget::addItem(); }
  }
  catch (PDOException $e) { $response = array('error', $e->getMessage()); }
  echo json_encode($response);
}


function deleteItem() {
  $response = array('success');

  try {
    if ($_GET['id'] != "") { MonthlyBudget::deleteItem($_GET['id']); }
    else { $response = array('error'); }
  }
  catch (PDOException $e) { $response = array('error', $e->getMessage()); }
  echo json_encode($response);
}

}

?>