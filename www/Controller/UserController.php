<?php

namespace ToddHudgens\MyFinancials\Controller;

use ToddHudgens\MyFinancials\Model\Account as Account;
use ToddHudgens\MyFinancials\Model\AccountType as AccountType;
use ToddHudgens\MyFinancials\Model\Asset as Asset;
use ToddHudgens\MyFinancials\Model\Transaction as Transaction;
use ToddHudgens\MyFinancials\Model\Twig as Twig;
use ToddHudgens\MyFinancials\Model\User as User;


class UserController {

function index() {
  global $view;
  $view['title'] = 'My Finances';
  $accounts = Account::getAll('accountType');
  $accountsTotal = 0;
  $assetsValue = 0;
  $accountLiabilities = 0;
  $accountTypes = AccountType::getAll();
  $assetsValue = Asset::totalValue();

  foreach ($accountTypes as $i => $type) { 
    if (isset($accounts[$type])) { 
      $total = 0;
      foreach ($accounts[$type] as $accountInfo) { $total += $accountInfo['balance']; }
      if ($total > 0) { $assetsValue += $total; } else { $accountLiabilities += $total; }
    }
  }
  $networth = $assetsValue + $accountLiabilities;

  $reports = array('report/asset-allocation' => "Asset Allocation",
                   'report/liquid-asset-allocation'=>"Liquid Asset Allocation",
                   'automobiles' => "Automobile Overview",
                   'report/gas-prices' => 'Gas Price Report',
                   'report/expenses' => 'Expense Report',
                   'report/expenses-by-category' => 'Expenses by Category',
                   'report/expenses-by-entity' => 'Expenses by Entity',
                   'report/expenses-by-tag' => 'Expenses by Tag',
                   'report/yearly-summary' => 'Yearly Cashflow Summary',
                   'report/networth' => 'Net Worth Report',
                   '/monthly-budget' => 'Monthly Budget',
                   'mortgage-calculator' => 'Mortgage Calculator');

  $expenses = Transaction::getTotalForFrontPage("Withdrawal", 30);
  $income = Transaction::getTotalForFrontPage("Deposit", 30);
  $topCategories = Transaction::getTotalsByCategory(30);

  $viewParams = array('title' => 'My Accounts',
                      'accounts' => $accounts,
                      'accountTypes' => $accountTypes,
                      'assetsValue' => $assetsValue,
		      'accountLiabilities' => $accountLiabilities,
                      'networth' => $networth,
                      'reports' => $reports,
                      'income' => $income,
                      'expenses' => $expenses,
                      'topCategories' => $topCategories);
  Twig::render('user-homepage.twig', $viewParams);
}


function login() {
  if (!loggedIn()) {
    if (User::total() > 0) {
      Twig::render('login-form.twig', array());
    }
    else { 
      Twig::render('user-init-add.twig', array());
    }
  }
  else { redirectToPage('/', 0); }
}



function loginSubmit() {
  $result = User::login($_POST['username'], $_POST['password']);
  if ($result) {
    Twig::render('login-success.twig', array());
    redirectToPage("/", 1);
  }
  else {
    Twig::render('login-failure.twig', array());
    redirectToPage('/user/login', 3);
  }
}



function logout() {
  if (session_status() === PHP_SESSION_ACTIVE) { session_destroy(); }
  Twig::render('logout.twig', array());
  redirectToPage('/', 2);
}


function initAddSubmit() {
  if (User::add($_POST)) {
    Twig::render('user-init-add-success.twig');
    redirectToPage('/', 4);
  }
  else {
    Twig::render('user-init-add-failure.twig');
  }
}



function sessionUpdate() {
  ini_set('session.gc_maxlifetime', 900);
  $_SESSION['LAST_ACTIVITY'] = time();
}


function sessionCheck() {
  if ((session_status() === PHP_SESSION_ACTIVE) &&
      isset($_SESSION['valid']) && ($_SESSION['valid'] == 1)) {
    $now = time();
    $secondsLeft = ($_SESSION['LAST_ACTIVITY'] + $GLOBALS['sessionTimeout']) - $now;
    echo $secondsLeft;
  }
  else {
    echo "false";
  }
}

}

?>
