<?php

namespace ToddHudgens\MyFinancials\Controller;

use ToddHudgens\MyFinancials\Model\Asset as Asset;
use ToddHudgens\MyFinancials\Model\Picture as Picture;
use ToddHudgens\MyFinancials\Model\Plugins as Plugins;


class PictureController {


function uploadAssetImages() {
  $response = Picture::uploadFiles($_FILES['files'], 'assets', $_REQUEST['entityId']);
  echo json_encode($response);
}


function uploadTransactionImages() {
  $response = Picture::uploadFiles($_FILES['files'], 'transactions', $_REQUEST['entityId']);
  echo json_encode($response);
}


function delete() {
  $response = Picture::delete($_REQUEST['id']); 
  echo json_encode($response);
}

}

?>