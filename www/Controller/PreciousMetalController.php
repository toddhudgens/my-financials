<?php

namespace ToddHudgens\MyFinancials\Controller;

use ToddHudgens\MyFinancials\Model\Asset as Asset;
use ToddHudgens\MyFinancials\Model\PreciousMetals as PreciousMetals;
use ToddHudgens\MyFinancials\Model\Twig as Twig;


class PreciousMetalController { 

function updatePrices() {
  $goldPrice = $_GET['gold'];
  $silverPrice = $_GET['silver'];
  echo $goldPrice . ',' . $silverPrice;

  // insert prices into database
  PreciousMetals::updatePrice("Gold", $goldPrice);
  PreciousMetals::updatePrice("Silver", $silverPrice);
  
  $results = PreciousMetals::getAssets();
  foreach ($results as $row) { 
    if ($row['metal'] === 'Silver') { $price = $silverPrice; }
    else if ($row['metal'] === 'Gold') { $price = $goldPrice; }
    $value = ($price * $row['weight'] * $row['purity'] * $row['quantity']) + 
             (($row['premium'] * $row['weight']) * $row['quantity']);
    Asset::updateCurrentValue($row['assetId'], $value);
  }
}


function assetInfo() {
  $row = PreciousMetals::getAsset($_GET['id']);
  if ($row) { echo json_encode($row); }
  else { echo json_encode(array()); }
}

}

?>