<?php

namespace ToddHudgens\MyFinancials\Controller;

use ToddHudgens\MyFinancials\Model\Stocks as Stocks;
use ToddHudgens\MyFinancials\Model\Transaction as Transaction;
use ToddHudgens\MyFinancials\Model\Twig as Twig;


class TransactionController { 

function delete() {
  $response = Transaction::delete($_GET['transactionId']);
  echo json_encode($response);
}



function search() {
  addJS('user/index.js');
  addCSS(array("user/index.css",'account.css'));
  $transactions = Transaction::search($_GET['s']);
  //echo '<pre>'; print_r($transactions); echo '</pre>';
  $viewParams = array('title' => "Transactions matching: ".$_GET['s'],
                      'searchString' => $_GET['s'],
                      'transactions' => $transactions);
  Twig::render('transaction-search-results.twig', $viewParams);
}



function save() {
  $txType = $_REQUEST['transactionType'];
  if ($txType == "Stock Purchase") { $response = Stocks::logTransaction($txType); }
  else if ($txType == "Stock Sale") { $response = Stocks::logTransaction($txType); }
  else if ($txType == "Transfer") { $response = Transaction::saveTransfer(); }
  else {  $response = Transaction::save(); } // deposits & withdrawals
  echo json_encode($response);
}

}

?>